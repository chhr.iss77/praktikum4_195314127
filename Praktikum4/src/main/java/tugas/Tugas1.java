package tugas;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.JTextField;

public class Tugas1 extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HIEGHT=200;
    private static final int FRAME_X_ORIGIN= 150;
    private static final int FRAME_y_ORIGIN= 250;
    private JLabel labeBil1;
    private JLabel labeBil2;
    private JLabel labeHasil;
    private JTextField text1;
    private JTextField text2;
    private JTextField text3;
    private JButton buttonJumlah;
    private int bil1, bil2, total;
    
    public static void main(String[] args) {
        Tugas1 frame = new Tugas1();
        frame.setVisible(true);
    }
    
    public Tugas1(){
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        
        setSize(FRAME_WIDTH, FRAME_HIEGHT);
        setResizable(true);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN, FRAME_y_ORIGIN);
        
        labeBil1 = new JLabel("Bilangan 1");
        labeBil1.setBounds(30, 15, 100, 50);
        contentPane.add(labeBil1);
        
        text1 = new JTextField();
        text1.setBounds(130, 30, 100, 20);
        contentPane.add(text1);
        
        labeBil2 = new JLabel("BIlangan 2");
        labeBil2.setBounds(30, 45, 100, 50);
        contentPane.add(labeBil2);
        
        text2 = new JTextField();
        text2.setBounds(130, 60, 100, 20);
        contentPane.add(text2);
        
        labeHasil = new JLabel("Hasil");
        labeHasil.setBounds(30, 75, 100, 50);
        contentPane.add(labeHasil);
        
        text3 = new JTextField();
        text3.setBounds(130, 90, 100, 20);
        contentPane.add(text3);
        
        buttonJumlah = new JButton("Jumlah");
        buttonJumlah.setBounds(130, 120, 80, 20);
        contentPane.add(buttonJumlah);
        
        buttonJumlah.addActionListener(this);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton tambah= (JButton) e.getSource();
        bil1 = Integer.parseInt(text1.getText());
        bil2 = Integer.parseInt(text2.getText());
        total= bil1+bil2;
        text3.setText(String.valueOf(total)); 
    }
    
}
